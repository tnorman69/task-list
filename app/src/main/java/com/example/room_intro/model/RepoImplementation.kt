package com.example.room_intro.model

import android.content.Context
import com.example.room_intro.util.StateResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class RepoImplementation(context: Context): Repo {
    private val todoDao = TodoDatabase.getDatabaseInstance(context).todoDao()

    override suspend fun addTodo(item: Todo) = withContext(Dispatchers.IO) {
        todoDao.addTodoItem(item)
    }

    override suspend fun editTodo(item: Todo) = withContext(Dispatchers.IO) {
        todoDao.addTodoItem(item)
    }

    override suspend fun fetchTodoList(): StateResource<List<Todo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = todoDao.fetchTodosAll()
            StateResource.Success(res)
        } catch(e: Exception) {
            StateResource.Error(e)
        }
    }

    override suspend fun fetchTodoByID(id: Int): StateResource<List<Todo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = todoDao.fetchTodoByID(id)
            StateResource.Success(res)
        } catch(e: Exception) {
            StateResource.Error(e)
        }
    }

    override suspend fun deleteByID(id: Int) = withContext(Dispatchers.IO) {
        todoDao.deleteByID(id)
    }

    override suspend fun clearDB() = withContext(Dispatchers.IO) {
        todoDao.clearDB()
    }

    override suspend fun fetchTodoListComplete(): StateResource<List<Todo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = todoDao.fetchTodosComplete()
            StateResource.Success(res)
        } catch(e: Exception) {
            StateResource.Error(e)
        }
    }

    override suspend fun fetchTodoListIncomplete(): StateResource<List<Todo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = todoDao.fetchTodosIncomplete()
            StateResource.Success(res)
        } catch(e: Exception) {
            StateResource.Error(e)
        }
    }

    override suspend fun completeTodo(id: Int) = withContext(Dispatchers.IO) {
        todoDao.completeTodo(id)
    }


}