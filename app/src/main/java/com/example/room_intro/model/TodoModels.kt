package com.example.room_intro.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "todo_item")
@Parcelize
data class Todo (
    @PrimaryKey val id: Int = 0,
    val title: String = "",
    val description: String = "",
    val completed: Boolean = false,
    val date: String = "",
    val updated: String = ""
): Parcelable

