package com.example.room_intro.model

import com.example.room_intro.util.StateResource

interface Repo {
    suspend fun addTodo(item: Todo)
    suspend fun editTodo(item: Todo)
    suspend fun fetchTodoList(): StateResource<List<Todo>>
    suspend fun fetchTodoByID(id: Int): StateResource<List<Todo>>
    suspend fun clearDB()
    suspend fun fetchTodoListComplete(): StateResource<List<Todo>>
    suspend fun fetchTodoListIncomplete(): StateResource<List<Todo>>
    suspend fun completeTodo(id: Int)
    suspend fun deleteByID(id: Int)
}

