package com.example.room_intro.model

import androidx.room.*

@Dao
interface TodoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTodoItem(item: Todo)

    @Query("SELECT * FROM todo_item")
    suspend fun fetchTodosAll(): List<Todo>

    @Query("SELECT * FROM todo_item WHERE completed")
    suspend fun fetchTodosComplete(): List<Todo>

    @Query("SELECT * FROM todo_item WHERE NOT completed")
    suspend fun fetchTodosIncomplete(): List<Todo>

    @Query("SELECT * FROM todo_item WHERE id = :id")
    suspend fun fetchTodoByID(id: Int): List<Todo>

    @Query("DELETE FROM todo_item")
    suspend fun clearDB()

    @Query("DELETE FROM todo_item WHERE id = :id")
    suspend fun deleteByID(id: Int)

    @Query("UPDATE todo_item SET completed = 1 WHERE id = :id")
    suspend fun completeTodo(id: Int)
}