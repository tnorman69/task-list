package com.example.room_intro.view

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.room_intro.R
import com.example.room_intro.databinding.TodoDetailFragmentBinding
import com.example.room_intro.genRandID
import com.example.room_intro.getTimeInstant
import com.example.room_intro.model.RepoImplementation
import com.example.room_intro.model.Todo
import com.example.room_intro.util.StateResource
import com.example.room_intro.viewmodel.DetailViewModel
import com.example.room_intro.viewmodel.DetailViewModelFactory

class TodoDetailFragment: Fragment() {
    private var _binding: TodoDetailFragmentBinding? = null
    private val binding: TodoDetailFragmentBinding get() = _binding!!
    private val repo by lazy {
        RepoImplementation(requireContext())
    }

    private val detailViewModel by lazy {
        ViewModelProvider(this, DetailViewModelFactory(repo))[DetailViewModel::class.java]
    }

    private val args by navArgs<TodoDetailFragmentArgs>()
    private val logTag = "DETAIL_FRAGMENT"

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = TodoDetailFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initViews() = with(binding) {
        var id = 0
        val timeUpdated: String by lazy { getTimeInstant().toString() }
        var timeCreated = ""

        when(args.mode) {
            "create" -> {
                id = genRandID().toInt()
                btnTodoConfirm.text = getString(R.string.create_todo)
                checkCompleted.isChecked = false
                inputDescription.setText("")
                inputTitle.setText("")
                timeCreated = getTimeInstant().toString()
            }
            "edit" -> {
                id = args.item!!.id
                btnTodoConfirm.text = getString(R.string.edit_todo)
                checkCompleted.isChecked = args.item!!.completed
                inputDescription.setText(args.item!!.description)
                inputTitle.setText(args.item!!.title)
                timeCreated = args.item!!.date
            }
            else -> {
                Log.d(logTag, "BAD NAV_ARGS (MODE)")
            }
        }

        btnTodoConfirm.setOnClickListener{
            val newItem = Todo(
                id = id,
                title = inputTitle.text.toString(),
                description = inputDescription.text.toString(),
                completed = checkCompleted.isChecked,
                date = timeCreated,
                updated = timeUpdated)
            detailViewModel.insertTodo(newItem)
        }

        detailViewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is StateResource.Error -> {
                    Log.d(logTag, it.err.toString())
                }
                is StateResource.Loading -> {
                    disableButtons()
                }
                is StateResource.Standby -> {
                    enableButtons()
                }
                is StateResource.Success -> {
                    detailViewModel.setStandby()
                    findNavController().navigateUp()
                }
            }
        }
    }

    private fun enableButtons() = with(binding) {
        progress.isVisible = false
        btnTodoConfirm.isEnabled = true
    }

    private fun disableButtons() = with(binding) {
        progress.isVisible = true
        btnTodoConfirm.isEnabled = false
    }
}