package com.example.room_intro.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.room_intro.R
import com.example.room_intro.databinding.TodoListFragmentBinding
import com.example.room_intro.model.RepoImplementation
import com.example.room_intro.util.StateResource
import com.example.room_intro.viewmodel.TodoListViewModel
import com.example.room_intro.viewmodel.TodoListViewModelFactory

class TodoListFragment: Fragment() {
    private var _binding: TodoListFragmentBinding? = null
    private val binding: TodoListFragmentBinding get() = _binding!!
    private val repo by lazy {
        RepoImplementation(requireContext())
    }

    private val listViewModel by lazy {
        ViewModelProvider(this, TodoListViewModelFactory(repo))[TodoListViewModel::class.java]
    }

    private val logTag = "LIST_FRAGMENT"

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = TodoListFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        val adapter = TodoListAdapter()

        btnCreate.setOnClickListener {
            val action = TodoListFragmentDirections.actionTodoListFragmentToTodoDetailFragment("create", null)
            findNavController().navigate(action)
        }

        btnClear.setOnClickListener {
            AlertDialog.Builder(requireContext())
            .setTitle("Clear To Do's")
            .setMessage("Are you sure you want to delete ALL tasks from your to do list?")
            .setPositiveButton(
            "Confirm",
                DialogInterface.OnClickListener { _, _ ->
                    listViewModel.clearDB()
            })
            .setNegativeButton("Cancel", null)
            .setIcon(R.drawable.baseline_error_outline_24)
            .show()
        }

        listViewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is StateResource.Error -> {
                    Log.d(logTag, it.err.toString())
                    listViewModel.setStandby()
                }
                is StateResource.Loading -> {
                    disableButtons()
                }
                is StateResource.Standby -> {
                    enableButtons()
                }
                is StateResource.Success -> {
                    adapter.setTodoList(it.data)

                    val swipeCallback = AdapterItemTouchCallback(adapter)
                    val swipeController = ItemTouchHelper(swipeCallback)
                    swipeController.attachToRecyclerView(recycler)

                    recycler.adapter = adapter
                    listViewModel.setStandby()
                }
            }
        }
        adapter.swipedTodo.observe(viewLifecycleOwner) {
            listViewModel.deleteByID(it.id)
        }

        listViewModel.fetchTodoList()
    }


    private fun enableButtons() = with(binding) {
        progress.isVisible = false
        btnCreate.isEnabled = true
    }

    private fun disableButtons() = with(binding) {
        progress.isVisible = true
        btnCreate.isEnabled = false
    }
}