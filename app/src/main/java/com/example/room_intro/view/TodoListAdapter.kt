package com.example.room_intro.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.room_intro.databinding.TodoCardBinding
import com.example.room_intro.model.Todo
import com.example.room_intro.usedIDs

class TodoListAdapter: RecyclerView.Adapter<TodoListAdapter.TodoViewHolder>() {
    private var todoList = mutableListOf<Todo>()
    private var _swipedTodo: MutableLiveData<Todo> = MutableLiveData<Todo>()
    val swipedTodo: LiveData<Todo> get() = _swipedTodo

    class TodoViewHolder(private val binding: TodoCardBinding): RecyclerView.ViewHolder(binding.root) {
        fun addItem(item: Todo) = with(binding) {
            usedIDs.add(item.id.toString())
            tvTitle.text = item.title
            checkbox.isChecked = item.completed
            binding.root.setOnClickListener {
                val action = TodoListFragmentDirections.actionTodoListFragmentToTodoDetailFragment("edit", item)
                binding.root.findNavController().navigate(action)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding = TodoCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TodoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        return holder.addItem(todoList[position])
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    fun setTodoList(items: List<Todo>) {
        todoList = items.toMutableList()
    }

    fun removeAt(position: Int) {
        Log.d("ADAPTER", position.toString())
        _swipedTodo!!.value = todoList[position]
    }
}