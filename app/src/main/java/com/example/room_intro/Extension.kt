package com.example.room_intro

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.Clock
import java.time.Instant
import kotlin.random.Random

var usedIDs = mutableListOf<String>()

@RequiresApi(Build.VERSION_CODES.O)
fun genRandID(len: Int = 8): String {
    var res = ""

    for(i in 1..len) {
        //res += Random(getTimeInstant().toEpochMilli()).nextInt(10)
        res += Random.nextInt(10)
    }

    return if(usedIDs.contains(res)) {
        genRandID(len)
    } else {
        res
    }
}

@RequiresApi(Build.VERSION_CODES.O)
fun getTimeInstant(): Instant {
    val clock: Clock = Clock.systemDefaultZone()
    val time = clock.instant().toString()
    return Instant.parse(time)
}