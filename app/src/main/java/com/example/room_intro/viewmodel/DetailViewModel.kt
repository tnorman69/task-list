package com.example.room_intro.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.room_intro.model.RepoImplementation
import com.example.room_intro.model.Todo
import com.example.room_intro.util.StateResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(val repo: RepoImplementation): ViewModel() {
    private var _state: MutableLiveData<StateResource<List<Todo>>> = MutableLiveData(StateResource.Standby())
    val state: LiveData<StateResource<List<Todo>>> get() = _state

    private fun setLoading() {
        _state.value = StateResource.Loading()
    }
    fun setStandby() {
        _state.value = StateResource.Standby()
    }
    private fun setBlankSuccess() {
        _state.value = StateResource.Success(listOf())
    }

    fun insertTodo(item: Todo) = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        repo.addTodo(item)
        setBlankSuccess()
    }
}