package com.example.room_intro.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.room_intro.model.RepoImplementation

class TodoListViewModelFactory(private val repo: RepoImplementation) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TodoListViewModel(repo) as T
    }
}