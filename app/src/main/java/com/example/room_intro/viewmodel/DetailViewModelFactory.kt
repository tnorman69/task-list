package com.example.room_intro.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.room_intro.model.RepoImplementation

class DetailViewModelFactory(private val repo: RepoImplementation) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailViewModel(repo) as T
    }
}