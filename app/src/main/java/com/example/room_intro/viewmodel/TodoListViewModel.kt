package com.example.room_intro.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.room_intro.model.RepoImplementation
import com.example.room_intro.model.Todo
import com.example.room_intro.util.StateResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TodoListViewModel(val repo: RepoImplementation): ViewModel() {
    private var _state: MutableLiveData<StateResource<List<Todo>>> = MutableLiveData(StateResource.Standby())
    val state: LiveData<StateResource<List<Todo>>> get() = _state

    private fun setLoading() {
        _state.value = StateResource.Loading()
    }
    fun setStandby() {
        _state.value = StateResource.Standby()
    }

    fun fetchTodoList() = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        _state.value = repo.fetchTodoList()
    }

    fun clearDB() = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        repo.clearDB()
        _state.value = repo.fetchTodoList()
    }

    fun fetchComplete() = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        _state.value = repo.fetchTodoListComplete()
    }
    fun fetchIncomplete() = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        _state.value = repo.fetchTodoListIncomplete()
    }
    fun deleteByID(id: Int) = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        repo.deleteByID(id)
        _state.value = repo.fetchTodoList()
    }
}